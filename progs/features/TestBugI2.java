class BugTest2 {
    public static void main(String[] a) {
	    System.out.println(new Test().two());
    }
}

class Test {
    public int two() {
        int x;
        x = 5;
        x++;  // Bug I2: Instead of incrementing x, set it to 1
        return x;
    }
}