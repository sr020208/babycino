class BugTest1 {
    public static void main(String[] a) {
	    System.out.println(new Test().one());
    }
}

class Test {
    public int one() {
        boolean x;
        x = true;
        x++;  // Bug I1: Incrementing boolean variable
        return x;
    }
}