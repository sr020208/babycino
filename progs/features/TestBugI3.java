class BugTest2 {
    public static void main(String[] a) {
	    System.out.println(new Test().three());
    }
}

class Test {
    public int three() {
        int x;
        x = 10;
        x++;  // Bug I3: Not modifying x at all
        return x;
    }
}